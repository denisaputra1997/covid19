package com.example.myapplication.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.Users;

import java.util.ArrayList;

import static android.text.TextUtils.isEmpty;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {

    //Deklarasi variabel
    private ArrayList<Users> listUserStatus;
    private Context context;

    //membuat kontruktor
    public StatusAdapter(ArrayList<Users> listUserStatus, Context context) {
        this.listUserStatus = listUserStatus;
        this.context = context;
    }

    //ViewHolder Digunakan untuk menyimpan referensi dari view
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView gejala, keterangan;
        private LinearLayout LinearItem;

        ViewHolder(View itemView) {
            super(itemView);

            //menginisiasi view yang terpasang pada layout recycle kita
            gejala = itemView.findViewById(R.id.valueStatus);
            keterangan = itemView.findViewById(R.id.valueKeterangan);
            LinearItem = itemView.findViewById(R.id.list_dataStatus);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //membuat view untuk menyiapkan dan memasang layout yang akan digunakan pada recycleview
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_status, parent, false);
        return new ViewHolder(V);
    }

    @SuppressLint("SetTextI18")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //mengambil nilai yang terdapat pada recycle view  berdasarkan posisi tertentu
        final String gejala = listUserStatus.get(position).getGejala();
        final String keterangan = listUserStatus.get(position).getKeterangan();

        //memasukan nilai/value kedalam view
        if (isEmpty(gejala)) {
            holder.gejala.setText("belum cek mandiri");
        } else {
            holder.gejala.setText(gejala);
        }

        if (isEmpty(keterangan)) {
            holder.keterangan.setText("belum cek mandiri");
        } else {
            holder.keterangan.setText(keterangan);
        }
    }

    @Override
    public int getItemCount() {

        //menghitung ukuran/jumlah data yang akan ditampilkan pada recycleview
        return listUserStatus.size();
    }
}

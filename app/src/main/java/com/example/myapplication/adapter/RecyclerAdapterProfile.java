package com.example.myapplication.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.Users;

import java.util.ArrayList;

import static android.text.TextUtils.isEmpty;

public class RecyclerAdapterProfile extends RecyclerView.Adapter<RecyclerAdapterProfile.ViewHolder> {

    //Deklarasi variabel
    private ArrayList<Users> listUser;
    private Context context;

    //membuat kontruktor
    public RecyclerAdapterProfile(ArrayList<Users> listUser, Context context) {
        this.listUser = listUser;
        this.context = context;
    }

    //ViewHolder Digunakan untuk menyimpan referensi dari view
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name, email, nik, telepon, kelamin, tanggal_lahir, alamat;
        private LinearLayout Listitem;

        ViewHolder(View itemView) {
            super(itemView);

            //menginisiasi view yang terpasang pada layout recycle kita
            name = itemView.findViewById(R.id.valueName);
            email = itemView.findViewById(R.id.valueEmail);
            nik = itemView.findViewById(R.id.valueNik);
            telepon = itemView.findViewById(R.id.valuePhone);
            kelamin = itemView.findViewById(R.id.valueKelamin);
            tanggal_lahir = itemView.findViewById(R.id.valueDate);
            alamat = itemView.findViewById(R.id.valueAddress);
            Listitem = itemView.findViewById(R.id.list_data);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //membuat view untuk menyiapkan dan memasang layout yang akan digunakan pada recycleview
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_design, parent, false);
        return new ViewHolder(V);
    }

    @SuppressLint("SetTextI18")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //mengambil nilai yang terdapat pada recycle view  berdasarkan posisi tertentu
        final String name = listUser.get(position).getName();
        final String email = listUser.get(position).getEmail();
        final String nik = listUser.get(position).getNik();
        final String telepon = listUser.get(position).getTelepon();
        final String kelamin = listUser.get(position).getKelamin();
        final String tanggal_lahir = listUser.get(position).getTanggal_lahir();
        final String alamat = listUser.get(position).getAlamat();

        //memasukan nilai/value kedalam view
        holder.name.setText(name);
        holder.email.setText(email);
        holder.nik.setText(nik);
        holder.telepon.setText(telepon);
        holder.kelamin.setText(kelamin);
        holder.tanggal_lahir.setText(tanggal_lahir);
        holder.alamat.setText(alamat);
    }

    @Override
    public int getItemCount() {

        //menghitung ukuran/jumlah data yang akan ditampilkan pada recycleview
        return listUser.size();
    }
}

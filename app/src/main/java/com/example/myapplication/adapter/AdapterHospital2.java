package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.models.GetDataHospital2;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterHospital2 extends RecyclerView.Adapter<AdapterHospital2.MyViewHolder>{
    List<GetDataHospital2> getDataHospitals;
    Context context;

    public AdapterHospital2(List<GetDataHospital2> getDataHospitals, Context context) {
        this.getDataHospitals = getDataHospitals;
        this.context = context;
    }


    @NonNull
    @NotNull
    @Override
    public AdapterHospital2.MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_listhospital, parent, false);
        return new AdapterHospital2.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterHospital2.MyViewHolder holder, int position) {
        holder.namaRs.setText(getDataHospitals.get(position).getNama_faskes());
        holder.alamatRs.setText(getDataHospitals.get(position).getAlamat());
        holder.teleponRs.setText(getDataHospitals.get(position).getNomor_telepon());
        holder.kabupatenRs.setText(getDataHospitals.get(position).getNama_kabupaten_kota());
    }

    @Override
    public int getItemCount() {
        return getDataHospitals.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView namaRs, alamatRs, teleponRs, kabupatenRs;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            namaRs = itemView.findViewById(R.id.namaRs);
            alamatRs = itemView.findViewById(R.id.alamatRs);
            teleponRs = itemView.findViewById(R.id.teleponRs);
            kabupatenRs = itemView.findViewById(R.id.kabupatenRs);
        }
    }
}

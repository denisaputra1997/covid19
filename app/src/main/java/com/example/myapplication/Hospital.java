package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.myapplication.adapter.AdapterHospital;
import com.example.myapplication.adapter.AdapterHospital2;
import com.example.myapplication.models.Faskes;
import com.example.myapplication.models.GetDataHospital;
import com.example.myapplication.models.GetDataHospital2;
import com.example.myapplication.rest.APIClient;
import com.example.myapplication.rest.APIInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Hospital extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);

        recyclerView = findViewById(R.id.ListRsRujukan);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        extractHospital();
    }

    private void extractHospital() {
        apiInterface = APIClient.getClient2().create(APIInterface.class);
        Call<Faskes> listCall =apiInterface.getDataHospital2();
        listCall.enqueue(new Callback<Faskes>() {
            @Override
            public void onResponse(Call<Faskes> call, Response<Faskes> response) {
                List<GetDataHospital2> getDataHospital2 = response.body().getGetDataHospital2();
                Log.d("SABISA", "onResponse: "+response.body());
                adapter = new AdapterHospital2(getDataHospital2, Hospital.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Faskes> call, Throwable t) {
                Log.d("GABISA", "onFailure: "+ t);
            }
        });
    }

    public void ivBack(View view) {
        startActivity(new Intent(Hospital.this, Dashboard.class));
        finish();
    }


}




package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    EditText etEmail,etPassword;
    FirebaseAuth fAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        fAuth = FirebaseAuth.getInstance();

        if (fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), Dashboard.class));
            finish();
        }
    }


    public void signUp(View view) {
        startActivity(new Intent(this, Signup.class));
    }

    public void signIn(View view) {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            etEmail.setError("Email is required.");
            return;
        }
        if (TextUtils.isEmpty(password)){
            etPassword.setError("Password is required.");
            return;
        }
        if (password.length() < 6){
            etPassword.setError("Password must be >= 6 characters");
            return;
        }

        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Toast.makeText(Login.this, "Logged is Successfully", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
            }else{
                Toast.makeText(Login.this, "Error! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setTitle("Menutup Aplikasi");
        builder.setMessage("Apakah anda ingin keluar dari aplikasi ini?");
        builder.setPositiveButton("ya",
                (dialog, which) ->  {
                    dialog.dismiss();
                    finishAffinity();
                });
        builder.setNegativeButton("tidak",
                (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.show();
    }
}
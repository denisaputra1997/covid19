package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapter.RecyclerAdapterProfile;
import com.example.myapplication.adapter.StatusAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Profile extends AppCompatActivity {

    //deklarasi variabel untuk recycleview
    private RecyclerView recyclerView, recyclerView1;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    //deklarasi variabel database reference dan arraylist dengan parameter class model kita
    private DatabaseReference reference;
    private ArrayList<Users> dataUsers;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        recyclerView = findViewById(R.id.datalist);
        recyclerView1 = findViewById(R.id.dataListStatus);
        auth = FirebaseAuth.getInstance();
        MyRecycleView();
        MyRecycleView1();
        GetData();
        GetDataStatus();

        //Initialize and Assign Variable
        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);

        //set home selected
        bottomNavigationView.setSelectedItemId(R.id.profile);

        //perform itemSelectedListener
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.home:
                    startActivity(new Intent(getApplicationContext(),Dashboard.class));
                    overridePendingTransition(0,0);
                    return true;
                case R.id.profile:
                    return true;
            }

            return false;
        });
    }

    //berisi baris kode untuk mengambil data dari database dan menampilkannya kedalam adapter
    private void GetData() {
        //mendapatkan reference database
        reference = FirebaseDatabase.getInstance("https://covid19-b37da-default-rtdb.firebaseio.com/").getReference();
        reference.child("Data").child(auth.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //inisialisasi arraylist
                dataUsers = new ArrayList<>();

                Users users = dataSnapshot.getValue(Users.class);
                users.setKey(dataSnapshot.getKey());
                dataUsers.add(users);

                //inisialisasi adapter dan data users dalam bentuk array
                adapter = new RecyclerAdapterProfile(dataUsers, Profile.this);

                //memasang adapter pada recycleview
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                //jika data gagal dimuat
                Toast.makeText(getApplicationContext(),"Data Gagal Dimuat", Toast.LENGTH_LONG).show();
                Log.e("activity_profile", error.getDetails()+" "+error.getMessage());
            }
        });
    }

    private void GetDataStatus() {
        //mendapatkan reference database
        reference = FirebaseDatabase.getInstance("https://covid19-b37da-default-rtdb.firebaseio.com/").getReference();
        reference.child("Data").child(auth.getUid()).child("alternative").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //inisialisasi arraylist
                dataUsers = new ArrayList<>();

                Users users = dataSnapshot.getValue(Users.class);
                users.setKey(dataSnapshot.getKey());
                dataUsers.add(users);

                //inisialisasi adapter dan data users dalam bentuk array
                adapter = new StatusAdapter(dataUsers, Profile.this);

                //memasang adapter pada recycleview
                recyclerView1.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                //jika data gagal dimuat
                Toast.makeText(getApplicationContext(),"Data Gagal Dimuat", Toast.LENGTH_LONG).show();
                Log.e("activity_profile", error.getDetails()+" "+error.getMessage());
            }
        });
    }

    private void MyRecycleView() {
        //menggunakan layout manager, dan membuat list secara vertical
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void MyRecycleView1() {
        //menggunakan layout manager, dan membuat list secara vertical
        layoutManager = new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(layoutManager);
        recyclerView1.setHasFixedSize(true);
    }

    public void logOut(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }
}
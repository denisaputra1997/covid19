package com.example.myapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.w3c.dom.Text;

public class GetDataHospital {

    @SerializedName("kode_kemkes")
    private int kode_kemkes;

    @SerializedName("nama")
    private String nama;

    @SerializedName("tipe_faskes")
    private int tipe_faskes;

    @SerializedName("rujukan")
    private String rujukan;

    @SerializedName("alamat")
    private String alamat;

    @SerializedName("telepon")
    private String telepon;

    @SerializedName("url")
    private String url;

    @SerializedName("longtitude")
    private String longtitude;

    @SerializedName("latitude")
    private String latitude;

    public GetDataHospital() {
    }

    public GetDataHospital(int kode_kemkes, String nama, int tipe_faskes, String rujukan, String alamat, String telepon, String url, String longtitude, String latitude) {
        this.kode_kemkes = kode_kemkes;
        this.nama = nama;
        this.tipe_faskes = tipe_faskes;
        this.rujukan = rujukan;
        this.alamat = alamat;
        this.telepon = telepon;
        this.url = url;
        this.longtitude = longtitude;
        this.latitude = latitude;
    }

    public int getKode_kemkes() {
        return kode_kemkes;
    }

    public void setKode_kemkes(int kode_kemkes) {
        this.kode_kemkes = kode_kemkes;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTipe_faskes() {
        return tipe_faskes;
    }

    public void setTipe_faskes(int tipe_faskes) {
        this.tipe_faskes = tipe_faskes;
    }

    public String getRujukan() {
        return rujukan;
    }

    public void setRujukan(String rujukan) {
        this.rujukan = rujukan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}

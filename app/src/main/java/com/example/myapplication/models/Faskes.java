package com.example.myapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Faskes {
    @SerializedName("data")
    private List<GetDataHospital2> getDataHospital2;

    public Faskes() {
    }

    public Faskes(List<GetDataHospital2> getDataHospital2) {
        this.getDataHospital2 = getDataHospital2;
    }

    public List<GetDataHospital2> getGetDataHospital2() {
        return getDataHospital2;
    }

    public void setGetDataHospital2(List<GetDataHospital2> getDataHospital2) {
        this.getDataHospital2 = getDataHospital2;
    }
}

package com.example.myapplication.models;

public class Bobot {
    private Integer bobot;

    public Bobot() {
    }

    public Bobot(Integer bobot) {
        this.bobot = bobot;
    }

    public Integer getBobot() {
        return bobot;
    }

    public void setBobot(Integer bobot) {
        this.bobot = bobot;
    }
}

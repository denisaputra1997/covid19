package com.example.myapplication.models;

import com.google.gson.annotations.SerializedName;

public class GetDataHospital2 {
    @SerializedName("nama_faskes")
    private String nama_faskes;

    @SerializedName("alamat")
    private String alamat;

    @SerializedName("nomor_telepon")
    private String nomor_telepon;

    @SerializedName("nama_kabupaten_kota")
    private String nama_kabupaten_kota;

    public GetDataHospital2() {
    }

    public GetDataHospital2(String nama_faskes, String alamat, String nomor_telepon, String nama_kabupaten_kota) {
        this.nama_faskes = nama_faskes;
        this.alamat = alamat;
        this.nomor_telepon = nomor_telepon;
        this.nama_kabupaten_kota = nama_kabupaten_kota;
    }

    public String getNama_faskes() {
        return nama_faskes;
    }

    public void setNama_faskes(String nama_faskes) {
        this.nama_faskes = nama_faskes;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNomor_telepon() {
        return nomor_telepon;
    }

    public void setNomor_telepon(String nomor_telepon) {
        this.nomor_telepon = nomor_telepon;
    }

    public String getNama_kabupaten_kota() {
        return nama_kabupaten_kota;
    }

    public void setNama_kabupaten_kota(String nama_kabupaten_kota) {
        this.nama_kabupaten_kota = nama_kabupaten_kota;
    }
}

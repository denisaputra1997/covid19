package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CovidUpdate extends AppCompatActivity {

    TextView update, valuekonfir, valueDiisolasi, valueSembuh, valueMeninggal;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_covid_update);

        update = findViewById(R.id.tvTitleForm);
        valuekonfir = findViewById(R.id.valueTerkonfirmasi);
        valueDiisolasi = findViewById(R.id.valueDiisolasi);
        valueSembuh = findViewById(R.id.valueSembuh);
        valueMeninggal = findViewById(R.id.valueMeninggal);

        mQueue = Volley.newRequestQueue(this);

        jsonParse();
    }

    public void ivBack(View view) {
        startActivity(new Intent(CovidUpdate.this, Dashboard.class));
        finish();
    }

    private void jsonParse() {

        String url = "https://covid19-public.digitalservice.id/api/v1/rekapitulasi_app/jabar_v2";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject DataArray = response.getJSONObject("data");
                            JSONObject metadataData = DataArray.getJSONObject("metadata");

                            JSONObject jsonArray = DataArray.getJSONObject("content");
//                            for (int i = 0 ; i <jsonArray.length(); i++) {
//                                JSONObject data = jsonArray.getJSONObject(i);
                            String last_update = metadataData.getString("last_update");
                            String confirmation_total = jsonArray.getString("confirmation_total");
                            String confirmation_diisolasi = jsonArray.getString("confirmation_diisolasi");
                            String confirmation_sembuh = jsonArray.getString("confirmation_sembuh");
                            String confirmation_meninggal = jsonArray.getString("confirmation_meninggal");


                            update.setText("Update Tanggal : " + last_update);
                            valuekonfir.setText(confirmation_total);
//                                valuekonfir.append(confirmation_total);
                            valueDiisolasi.setText(confirmation_diisolasi);
                            valueSembuh.setText(confirmation_sembuh);
                            valueMeninggal.setText(confirmation_meninggal);


//                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}
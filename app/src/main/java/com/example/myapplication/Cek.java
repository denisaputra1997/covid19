package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.example.myapplication.models.Bobot;
import com.example.myapplication.models.UserRegister;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class Cek extends AppCompatActivity {

    ScrollView content;
    RadioGroup rgDemam, rgBatuk, rgTenggorokan, rgHidung, rgKepala, rgNyeriOtot, rgSesakNapas, rgNapasCepat, rgDemamTinggi, rgBatukKering, rgInfeksiNapas, rgNapasSangatCepat, rgPernapasanBerat, rgPPOK, rgKolapsLobus;
    RadioButton rbIyaDemam, rbIyaBatuk, rbIyaTenggorokan, rbIyaHidung, rbIyaKepala, rbIyaNyeriOtot, rbIyaSesakNapas, rbIyaNapasCepat, rbIyaDemamTinggi, rbIyaBatukKering, rbIyaInfeksiNapas, rbIyaNapasSangatCepat, rbIyaPernapasanBerat, rbIyaPPOK, rbIyaKolapsLobus, rbTidakDemam, rbTidakBatuk, rbTidakTenggorokan, rbTidakHidung, rbTidakKepala, rbTidakNyeriOtot, rbTidakSesakNapas, rbTidakNapasCepat, rbTidakDemamTinggi, rbTidakBatukKering, rbTidakInfeksiNapas, rbTidakNapasSangatCepat, rbTidakPernapasanBerat, rbTidakPPOK, rbTidakKolapsLobus;
    Double bobot = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0, c4 = 0.0, c5 = 0.0, c6 = 0.0, c7 = 0.0, c8 = 0.0, c9 = 0.0, c10 = 0.0, c11 = 0.0, c12 = 0.0, c13 = 0.0, c14 = 0.0, c15 = 0.0;
    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference getReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek);

        content = findViewById(R.id.conten_cek);

        rgDemam = findViewById(R.id.rgDemam);
        rgBatuk = findViewById(R.id.rgBatuk);
        rgTenggorokan = findViewById(R.id.rgTenggorokan);
        rgHidung = findViewById(R.id.rgHidung);
        rgKepala = findViewById(R.id.rgKepala);
        rgNyeriOtot = findViewById(R.id.rgNyeriOtot);
        rgSesakNapas = findViewById(R.id.rgSesakNapas);
        rgNapasCepat = findViewById(R.id.rgNapasCepat);
        rgDemamTinggi = findViewById(R.id.rgDemamTinggi);
        rgBatukKering = findViewById(R.id.rgBatukKering);
        rgInfeksiNapas = findViewById(R.id.rgInfeksiNapas);
        rgNapasSangatCepat = findViewById(R.id.rgNapasSangatCepat);
        rgPernapasanBerat = findViewById(R.id.rgSesakNapas);
        rgPPOK = findViewById(R.id.rgPenyakitParu);
        rgKolapsLobus = findViewById(R.id.rgKolapsLobus);

    }

    public void ivBack(View view) {
        startActivity(new Intent(getApplicationContext(), Dashboard.class));
        finish();
    }

    public void simpan(View view) {
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance("https://covid19-b37da-default-rtdb.firebaseio.com/");
        getReference = database.getReference();
        Map<String, Object> updates = new HashMap<>();
//        updates.put("bobot", bobot);
        updates.put("c1", c1);
        updates.put("c2", c2);
        updates.put("c3", c3);
        updates.put("c4", c4);
        updates.put("c5", c5);
        updates.put("c6", c6);
        updates.put("c7", c7);
        updates.put("c8", c8);
        updates.put("c9", c9);
        updates.put("c10", c10);
        updates.put("c11", c11);
        updates.put("c12", c12);
        updates.put("c13", c13);
        updates.put("c14", c14);
        updates.put("c15", c15);
        getReference.child("Data").child(auth.getUid()).child("topsis").updateChildren(updates)
            .addOnSuccessListener(this, (OnSuccessListener) o -> {
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
                finish();
            });
    }

    public void onRadioButtonClicked(View view) {

        //is the button checked?
        boolean checked = ((RadioButton) view).isChecked();

        //check which radio button was clicked
        switch (view.getId()) {
            case R.id.rbTidakDemam:
            case R.id.rbTidakBatuk:
            case R.id.rbTidakTenggorokan:
            case R.id.rbTidakHidung:
            case R.id.rbTidakKepala:
            case R.id.rbTidakNyeriOtot:
            case R.id.rbTidakSesakNapas:
            case R.id.rbTidakNapasCepat:
            case R.id.rbTidakDemamTinggi:
            case R.id.rbTidakBatukKering:
            case R.id.rbTidakInfeksiNapas:
            case R.id.rbTidakNapasSangatCepat:
            case R.id.rbTidakPernapasanBerat:
            case R.id.rbTidakPenyakitParu:
            case R.id.rbTidakKolapsLobus:
                if (checked)
                    bobot = bobot + 0.0;
                break;
            case R.id.rbIyaDemam:
                if (checked)
                    c1 = c1 + 0.030043272;
                    bobot = bobot + c1;
                break;
            case R.id.rbIyaBatuk:
                if (checked)
                    c2 = c2 + 0.023175324;
                    bobot = bobot + c2;
                break;
            case R.id.rbIyaTenggorokan:
                if (checked)
                    c3 = c3 + 0.019038104;
                    bobot = bobot + c3;
                break;
            case R.id.rbIyaHidung:
                if (checked)
                    c4 = c4 + 0.013436589;
                    bobot = bobot + c4;
                break;
            case R.id.rbIyaKepala:
                if (checked)
                    c5 = c5 + 0.010196972;
                    bobot = bobot + c5;
                break;
            case R.id.rbIyaNyeriOtot:
                if (checked)
                    c6 = c6 + 0.009883349;
                    bobot = bobot + c6;
                break;
            case R.id.rbIyaSesakNapas:
                if (checked)
                    c7 = c7 + 0.045805421;
                    bobot = bobot + c7;
                break;

            case R.id.rbIyaNapasCepat:
                if (checked)
                    c8 = c8 + 0.035577541;
                    bobot = bobot + c8;
                break;
            case R.id.rbIyaDemamTinggi:
                if (checked)
                    c9 = c9 + 0.039869601;
                    bobot = bobot + c9;
                break;
            case R.id.rbIyaBatukKering:
                if (checked)
                    c10 = c10 + 0.053049746;
                    bobot = bobot + c10;
                break;

            case R.id.rbIyaInfeksiNapas:
                if (checked)
                    c11 = c11 + 0.084586716;
                    bobot = bobot + c11;
                break;

            case R.id.rbIyaNapasSangatCepat:
                if (checked)
                    c12 = c12 + 0.091602707;
                    bobot = bobot + c12;
                break;

            case R.id.rbIyaPernapasanBerat:
                if (checked)
                    c13 = c13 + 0.129779655;
                    bobot = bobot + c13;
                break;

            case R.id.rbIyaPenyakitParu:
                if (checked)
                    c14 = c14 + 0.183061665;
                    bobot = bobot + c14;
                break;

            case R.id.rbIyaKolapsLobus:
                if (checked)
                    c15 = c15 + 0.230893338;
                    bobot = bobot + c15;
                break;
        }
    }
}
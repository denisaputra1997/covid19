package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication.models.UserRegister;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.text.TextUtils.isEmpty;

public class Signup extends AppCompatActivity{

    EditText etRegName, etRegEmail , etRegPass, etRegNik, etRegBirthday, etRegPhone, etRegAddress;
    Spinner etJenisKelamin;
    Button btRegSignUp;
    FirebaseAuth fAuth;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        etRegName = findViewById(R.id.etRegName);
        etRegEmail = findViewById(R.id.etRegEmail);
        etRegPass = findViewById(R.id.etRegPass);
        etRegNik = findViewById(R.id.etRegNik);
        etRegBirthday = findViewById(R.id.etRegBirthday);
        etJenisKelamin = findViewById(R.id.etJenisKelamin);
        etRegPhone = findViewById(R.id.etRegPhone);
        etRegAddress = findViewById(R.id.etRegAddress);

        fAuth = FirebaseAuth.getInstance();

        if (fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), Dashboard.class));
            finish();
        }

        myCalendar = Calendar.getInstance();

        date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(etRegBirthday);
        };

        etRegBirthday.setOnClickListener(v -> {
            showCalendar(date);
        });
    }

    private void showCalendar(DatePickerDialog.OnDateSetListener date) {
        DatePickerDialog dialog = new DatePickerDialog(
                this,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    private void updateLabel(EditText editText) {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText.setText(sdf.format(myCalendar.getTime()));
    }

    public void signUp(View view) {

        String name = etRegName.getText().toString().trim();
        String email = etRegEmail.getText().toString().trim();
        String password = etRegPass.getText().toString().trim();

        if (TextUtils.isEmpty(name)){
            etRegName.setError("Name is Required.");
            return;
        }

        if (TextUtils.isEmpty(email)){
            etRegEmail.setError("Email is required.");
            return;
        }
        if (TextUtils.isEmpty(password)){
            etRegPass.setError("Password is required.");
            return;
        }
        if (password.length() < 6){
            etRegPass.setError("Password must be >= 6 characters");
            return;
        }

        //register the user in firebase
        fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Toast.makeText(Signup.this, "Register Success! ", Toast.LENGTH_SHORT).show();
                createUserData(email, password);
            }else{
                Toast.makeText(Signup.this, "Register Failed! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createUserData(String email, String password) {
        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                String getUserID = fAuth.getCurrentUser().getUid();
                FirebaseDatabase database = FirebaseDatabase.getInstance("https://covid19-b37da-default-rtdb.firebaseio.com/");
                DatabaseReference getReference;

                String getName = etRegName.getText().toString();
                String getEmail = etRegEmail.getText().toString();
                String getNik = etRegNik.getText().toString();
                String getTelepon = etRegPhone.getText().toString();
                String getKelamin = etJenisKelamin.getSelectedItem().toString();
                String getTanggal_lahir = etRegBirthday.getText().toString();
                String getAlamat = etRegAddress.getText().toString();

                getReference = database.getReference();

                if (isEmpty(getName) && isEmpty(getEmail) && isEmpty(getNik) && isEmpty(getTelepon) && isEmpty(getKelamin) && isEmpty(getTanggal_lahir) && isEmpty(getAlamat)){
                    Toast.makeText(this, "Data Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }else{
                    getReference.child("Data").child(getUserID)
                        .setValue(new UserRegister(getName, getEmail, getNik, getTelepon, getKelamin, getTanggal_lahir, getAlamat))
                        .addOnSuccessListener(this, (OnSuccessListener) o -> {
                            startActivity(new Intent(getApplicationContext(), Dashboard.class));
                            finish();
                        });
                }

            }else{
                Toast.makeText(this, "Error! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void signIn(View view) {
        startActivity(new Intent(Signup.this, Login.class));
    }
}
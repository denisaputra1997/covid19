package com.example.myapplication;

public class Users {
    private String key;
    private String name;
    private String email;
    private String nik;
    private String telepon;
    private String kelamin;
    private String tanggal_lahir;
    private String alamat;
    private String gejala;
    private String keterangan;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getKelamin() {
        return kelamin;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getGejala() {
        return gejala;
    }

    public void setGejala(String gejala) {
        this.gejala = gejala;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Users() {
    }

    public Users(String key, String name, String email, String nik, String telepon, String kelamin, String tanggal_lahir, String alamat, String gejala, String keterangan) {
        this.key = key;
        this.name = name;
        this.email = email;
        this.nik = nik;
        this.telepon = telepon;
        this.kelamin = kelamin;
        this.tanggal_lahir = tanggal_lahir;
        this.alamat = alamat;
        this.gejala = gejala;
        this.keterangan = keterangan;
    }
}

package com.example.myapplication.rest;

import com.example.myapplication.models.Faskes;
import com.example.myapplication.models.GetDataHospital;
import com.example.myapplication.models.GetDataHospital2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("faskes")
    Call<List<GetDataHospital>> getDataHospital();

    @GET("od_daftar_fasilitas_kesehatan")
    Call<Faskes> getDataHospital2();
}

package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

public class CovidInfo extends AppCompatActivity {

    TextView textLink, textLinkBekasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_covid_info);

        textLinkBekasi = findViewById(R.id.tvLinkBekasi);
        textLink = findViewById(R.id.tvLink);
        textLink.setMovementMethod(LinkMovementMethod.getInstance());
        textLinkBekasi.setMovementMethod(LinkMovementMethod.getInstance());


    }

    public void backInfo(View view) {
        startActivity(new Intent(CovidInfo.this, Dashboard.class));
        finish();
    }


}
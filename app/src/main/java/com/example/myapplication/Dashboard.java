package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Dashboard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //Initialize and Assign Variable

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);

        //set home selected
        bottomNavigationView.setSelectedItemId(R.id.home);

        //perform itemSelectedListener
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.home:
                    return true;
                case R.id.profile:
                    startActivity(new Intent(getApplicationContext()
                            ,Profile.class));
                    overridePendingTransition(0,0);
                    return true;
            }
            return false;
        });
    }

    public void checkUp(View view) {
        Intent intentCheck = new Intent(this, Cek.class);
        startActivity(intentCheck);
    }

    public void hospital(View view) {
        Intent intentHospital = new Intent(this, Hospital.class);
        startActivity(intentHospital);
    }

    public void covidInfo(View view) {
        Intent intentCovidInfo = new Intent(this, CovidInfo.class);
        startActivity(intentCovidInfo);
    }

    public void covidUpdate(View view) {
        Intent intentCovidUpdate = new Intent(this, CovidUpdate.class);
        startActivity(intentCovidUpdate);
    }
}